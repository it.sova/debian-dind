#!/usr/bin/env bash
[ -z $1 ] && REPO=https://gitlab.com/it.sova/docker-nginx-with-lua.git || REPO=$1
[ -z $2 ] && RUNNER=build_and_run.sh || RUNNER=$2

CONTAINER_NAME=debian-dind

sudo docker build ./docker -t ${CONTAINER_NAME} && \
# Keep overlay directory out of build context
# to speedup image build ( get rid of "Sending context to daemon..." phase )
mkdir -p overlay && \
docker run \
    --rm --privileged -it \
    -e REPO=${REPO} \
    -e RUNNER=${RUNNER} \
    --name ${CONTAINER_NAME} \
    -v $(pwd)/overlay:/var/lib/docker \
    ${CONTAINER_NAME}
