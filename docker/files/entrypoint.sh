#!/usr/bin/env bash

nohup dockerd &>/dev/null &

while ! docker ps -q; do
    echo "Waiting for dockerd to start";
    sleep 1;
done

git clone ${REPO} .
find ./ -type f -name ${RUNNER} -exec bash "{}" \;
