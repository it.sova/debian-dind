### Custom Docker-in-Docker image based on Debian 11

## Description
Image based on Debian 11. Container should be started in privileged mode. Entrypoint script will pull remote repo from ${REPO} env. variable and run builder\runner script from ${RUNNER} env. variable. Default `./run.sh` script also creates and mounts directory to `/var/lib/docker` in container to preserve data directory of nested Docker.
## Usage
```
./run.sh REPO_URL RUNNER to build and run image. If no positional arguments provided - defaults will be used.
```